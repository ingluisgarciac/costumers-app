import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AppFrame from './../components/AppFrame';
import CustomerEdit from '../components/CustomerEdit';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {insertCustomer} from './../actions/insertCustomer';
import { SubmissionError } from 'redux-form';

class NewCustomerContainer extends Component {

    handleSubmit = values =>{
        return this.props.insertCustomer(values).then(r =>{
            if(r.error){
                throw new SubmissionError(r.payload);
            }
        });
    }

    handleSubmitSuccess =() =>{
        this.props.history.goBack();
    }

    handleOnBack =()=>{
        this.props.history.goBack();
    }

    renderBody =() =>{
        return <CustomerEdit onSubmit={this.handleSubmit}
        onSubmitSuccess={this.handleSubmitSuccess}
        onBack = {this.handleOnBack}></CustomerEdit>
    }

    render() {
        return (
            <div>
                <AppFrame header={`Creación del nuevo cliente`}
                body={
                    this.renderBody()
                }></AppFrame>
            </div>
        );
    }
}

NewCustomerContainer.propTypes = {
    insertCustomer:PropTypes.func.isRequired,
};

export default withRouter(connect(null, {insertCustomer})(NewCustomerContainer));