import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import HomeContainer from './containers/HomeContainer';
import CustomersContainer from './containers/CustomersContainer';
import CustomerContainer from './containers/CustomerContainer';
import NewCustomerContainer from './containers/NewCustomerContainer';

class App extends Component {

  renderHome =() => <HomeContainer />;

  renderCustomerContainder =() => <CustomersContainer />;

  renderCustomerListContainer =()=><h1>Customer List Container</h1>;

  render() {
    console.log("me renderizo");
    return (
      <Router >
        <div className="App">
          <Route exact path="/" component={this.renderHome}></Route>
          <Route exact path="/customers" component={this.renderCustomerContainder}></Route>
          <Switch>
            <Route path="/customers/new" component={NewCustomerContainer}></Route>
            <Route path="/customers/:dni" 
            render={props => <CustomerContainer dni={props.match.params.dni}/>}></Route>
          </Switch>
        </div>
      </Router>
    );
  }
}


export default App;
